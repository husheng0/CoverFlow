using UnityEngine;
using System.Collections;

[RequireComponent( typeof( AudioSource ) ) ]
public class TabButtonUniflowGalleryControl : ButtonTemplate, ITabButtonEventListener
{
	public GameObject buttonDescriptionText;
	public float descriptionTextEffectDuration;

	public int thumbnailToGo;
	public UniflowGallery uniflowGalleryComponent;

	private UIEffectColor m_rColorDescriptionTextEffect;

	private Color m_oSavedDescriptionTextColor;

	public override void Awake ()
	{
		base.Awake( );

		if( buttonDescriptionText != null )
		{
			m_oSavedDescriptionTextColor = buttonDescriptionText.renderer.material.color;
			m_rColorDescriptionTextEffect = buttonDescriptionText.AddComponent<UIEffectColor>( ) as UIEffectColor;
			m_rColorDescriptionTextEffect.Init( );
			m_rColorDescriptionTextEffect.easing = gkInterpolate.EaseType.EaseOutCirc;
			m_rColorDescriptionTextEffect.duration = labelEffectDuration;
	
			m_rColorDescriptionTextEffect.colorOut = m_oSavedDescriptionTextColor;
	
			Color oTransparentDescriptionTextColor = m_oSavedDescriptionTextColor;
			oTransparentDescriptionTextColor.a = 0.0f;
			m_rColorDescriptionTextEffect.colorIn = oTransparentDescriptionTextColor;
		}
	}

	public override void Start( )
	{
		uniflowGalleryComponent.onSelectedThumbnailUpdateHandler += new UniflowGallery.OnSelectedThumbnailUpdate( this.OnSelectedThumbnailUpdate );

		if( m_bHasBeenClicked == false )
		{
			m_rScaleLabelEffect.ResetAndStop( );
			m_rScaleBackgroundEffect.ResetAndStop( );
			if( m_rColorDescriptionTextEffect != null )
			{
				m_rColorDescriptionTextEffect.ResetAndStop( );
			}
		}
	}

	public override void OnClickEvent( )
	{
		if( m_bHasBeenClicked == false )
		{
			transform.parent.SendMessage( "OnTabButtonClickedEvent", this );
		}
	}

	public void OnTabButtonSelectedEvent( )
	{
		m_bHasBeenClicked = true;

		PlayTextEffect( true );
		PlayBackgroundEffect( true );
		PlayDescriptionTextEffect( true );

		if( uniflowGalleryComponent.selectedThumbnail != thumbnailToGo )
		{
			PlayClickSound( );
			uniflowGalleryComponent.selectedThumbnail = thumbnailToGo;
		}
	}

	public void OnTabButtonDeselectedEvent( )
	{
		PlayTextEffect( false );
		PlayBackgroundEffect( false );
		PlayDescriptionTextEffect( false );

		m_bHasBeenClicked = false;
	}

	private void PlayDescriptionTextEffect( bool a_bPlayForwards )
	{
		if( m_rColorDescriptionTextEffect != null )
		{
			m_rColorDescriptionTextEffect.Pause( );
			m_rColorDescriptionTextEffect.colorIn = m_rColorDescriptionTextEffect.currentColor;
	
			if( a_bPlayForwards == true )
			{
				m_rColorDescriptionTextEffect.colorOut = m_oSavedDescriptionTextColor;
			}
			else
			{
				Color oTransparentDescriptionTextColor = m_oSavedDescriptionTextColor;
				oTransparentDescriptionTextColor.a = 0.0f;
				m_rColorDescriptionTextEffect.colorOut = oTransparentDescriptionTextColor;			
			}
	 
			m_rColorDescriptionTextEffect.Restart( );
		}
	}

	// Simulates a click when the current selected gallery thumbnail is
	// the same as the thumbnail associated to this tab button
	private void OnSelectedThumbnailUpdate( UniflowGallery a_rGallery, UniflowThumbnail a_rThumbnailPrevious, UniflowThumbnail a_rThumbnailCurrent )
	{
		if( a_rThumbnailCurrent.thumbnailIndex == thumbnailToGo && m_bHasBeenClicked == false )
		{
			transform.parent.SendMessage( "OnTabButtonClickedEvent", this );
		}
	}
}
