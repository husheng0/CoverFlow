using UnityEngine;
using System.Collections;

[RequireComponent( typeof( BoxCollider ) ) ]
[RequireComponent( typeof( AudioSource ) ) ]

public class ButtonTemplate : MonoBehaviour, IClickEventListener {

	public GameObject buttonBackground;
	public GameObject buttonLabel;

	// Scale effect
	public float backgroundScaleEffectFactor;
	public float labelScaleEffectFactor;

	// Effect duration
	public float backgroundEffectDuration;
	public float labelEffectDuration;

	// UI Effect components
	protected UIEffectScaleFactor m_rScaleLabelEffect;
	protected UIEffectScaleFactor m_rScaleBackgroundEffect;

	protected bool m_bHasBeenClicked;

	public virtual void Awake ()
	{
		if( buttonLabel != null )
		{
			m_rScaleLabelEffect = buttonLabel.AddComponent<UIEffectScaleFactor>( ) as UIEffectScaleFactor;
			m_rScaleLabelEffect.Init( );
			m_rScaleLabelEffect.easing = gkInterpolate.EaseType.EaseOutCirc;
			m_rScaleLabelEffect.duration = labelEffectDuration;
			m_rScaleLabelEffect.beginningScaleFactorScalar = 1.0f;
			m_rScaleLabelEffect.finalScaleFactorScalar = labelScaleEffectFactor;
		}

		if( buttonBackground != null )
		{
			m_rScaleBackgroundEffect = buttonBackground.AddComponent<UIEffectScaleFactor>( ) as UIEffectScaleFactor;
			m_rScaleBackgroundEffect.Init( );
			m_rScaleBackgroundEffect.easing = gkInterpolate.EaseType.EaseOutCirc;
			m_rScaleBackgroundEffect.duration = backgroundEffectDuration;
			m_rScaleBackgroundEffect.beginningScaleFactor = new Vector3( 0.0f, 1.0f, 1.0f );
			m_rScaleBackgroundEffect.finalScaleFactor = new Vector3( backgroundScaleEffectFactor, 1.0f, 1.0f );
		}
	}

	public virtual void Start( )
	{
		if( m_rScaleLabelEffect != null )
		{
			m_rScaleLabelEffect.ResetAndStop( );
		}

		if( m_rScaleBackgroundEffect != null )
		{
			m_rScaleBackgroundEffect.ResetAndStop( );
		}
		m_bHasBeenClicked = false;
	}

	public virtual void OnClickEvent( )
	{
		m_bHasBeenClicked = true;

		if( buttonLabel != null )
		{
			PlayTextEffect( true );
		}

		if( buttonBackground != null )
		{
			PlayBackgroundEffect( true );
		}

		PlayClickSound( );
	}

	public virtual void OnHoverEvent( bool a_bIsHovered )
	{
		if( m_bHasBeenClicked == false && buttonLabel != null )
		{
			PlayTextEffect( a_bIsHovered );
		}
	}

	protected virtual void PlayTextEffect( bool a_bPlayForwards )
	{
		m_rScaleLabelEffect.Pause( );
		m_rScaleLabelEffect.beginningScaleFactor = m_rScaleLabelEffect.currentScaleFactor;

		if( a_bPlayForwards == true )
		{
			m_rScaleLabelEffect.finalScaleFactorScalar = labelScaleEffectFactor;
		}
		else
		{
			m_rScaleLabelEffect.finalScaleFactorScalar = 1.0f;			
		}

		m_rScaleLabelEffect.Restart( );
	}

	protected virtual void PlayBackgroundEffect( bool a_bPlayForwards )
	{
		m_rScaleBackgroundEffect.Pause( );
		m_rScaleBackgroundEffect.beginningScaleFactor = m_rScaleBackgroundEffect.currentScaleFactor;

		if( a_bPlayForwards == true )
		{
			m_rScaleBackgroundEffect.finalScaleFactor = new Vector3( backgroundScaleEffectFactor, 1.0f, 1.0f );
		}
		else
		{
			m_rScaleBackgroundEffect.finalScaleFactor = new Vector3( 0.0f, 1.0f, 1.0f );			
		}
 
		m_rScaleBackgroundEffect.Restart( );
	}

	protected virtual void PlayClickSound( )
	{
		AudioSource rAudioSource = gameObject.GetComponent<AudioSource>( ) as AudioSource;
		if( rAudioSource != null && rAudioSource.clip != null )
		{
			rAudioSource.Play( );
		}
	}
}