using UnityEngine;
using System.Collections;

public class UIEffectScaleFactor : UIEffectTemplate
{
	private Vector3 m_f3SavedInitialLocalScale;
	private Vector3 m_f3BeginningScaleFactor;
	private Vector3 m_f3FinalScaleFactor;

	public Vector3 finalScaleFactor
	{
		get
		{
			return m_f3FinalScaleFactor;
		}
		set
		{
			m_f3FinalScaleFactor = value;
		}
	}

	public float finalScaleFactorScalar
	{
		set
		{
			m_f3FinalScaleFactor = value * Vector3.one;
		}
	}

	public Vector3 beginningScaleFactor
	{
		get
		{
			return m_f3BeginningScaleFactor;
		}
		set
		{
			m_f3BeginningScaleFactor = value;
		}
	}

	public float beginningScaleFactorScalar
	{
		set
		{
			m_f3BeginningScaleFactor = value * Vector3.one;
		}
	}	

	public Vector3 currentScaleFactor
	{
		get
		{
			return gkInterpolate.Ease( m_dEasingFunction,
			                          m_f3BeginningScaleFactor,
			                          m_f3FinalScaleFactor - m_f3BeginningScaleFactor,
			                          m_fTimer,
			                          m_fDuration );
		}
	}

	protected override void InternalInit( )
	{
		base.InternalInit( );
		m_f3SavedInitialLocalScale = transform.localScale;
		m_f3BeginningScaleFactor = Vector3.one;
		m_f3FinalScaleFactor = Vector3.one;
	}

	protected override void EffectUpdate( )
	{
		Vector3 f3CurrentScaleFactor = gkInterpolate.Ease( m_dEasingFunction, m_f3BeginningScaleFactor, m_f3FinalScaleFactor - m_f3BeginningScaleFactor, m_fTimer, m_fDuration );

		transform.localScale = Vector3.Scale( f3CurrentScaleFactor, m_f3SavedInitialLocalScale );
	}
}
