using UnityEngine;
using System.Collections;

public class UIEffectTransformRelativeCoords : UIEffectTemplate
{
	protected Transform m_rCachedTransform;

	protected Vector3 m_f3SavedPosition;
	protected Vector3 m_f3SavedRotation;
	protected Vector3 m_f3SavedScale;

	protected Vector3 m_f3FinalPositionOffset;
	protected Vector3 m_f3FinalRotationOffset;
	protected Vector3 m_f3FinalScaleOffset;

	protected Vector3 m_f3PreviousPositionOffset;
	protected Vector3 m_f3PreviousRotationOffset;
	protected Vector3 m_f3PreviousScaleOffset;

	// Treated as a position
	public Vector3 startPosition
	{
		set
		{
			m_f3SavedPosition = value;
		}
	}

	public Quaternion startRotation
	{
		set
		{
			m_f3SavedRotation = value.eulerAngles;
		}
	}

	// Treated as an offset
	public Vector3 finalPositionOffset
	{
		set
		{
			m_f3PreviousPositionOffset = gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousPositionOffset, m_f3FinalPositionOffset - m_f3PreviousPositionOffset, m_fTimer, m_fDuration );
			m_f3FinalPositionOffset = value;
		}
	}

	// Treated as an offset
	public Quaternion finalRotationOffset
	{
		set
		{
			m_f3PreviousRotationOffset = gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousRotationOffset, m_f3FinalRotationOffset - m_f3PreviousRotationOffset, m_fTimer, m_fDuration );
			m_f3FinalRotationOffset = value.eulerAngles;
		}
	}

	// Treated as an offset
	public Vector3 finalEulerAnglesOffset
	{
		set
		{
			Vector3 f3EulerAngles = gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousRotationOffset, m_f3FinalRotationOffset - m_f3PreviousRotationOffset, m_fTimer, m_fDuration );
			m_f3PreviousRotationOffset = f3EulerAngles;
			m_f3FinalRotationOffset = value;
		}
	}

	public Vector3 finalScaleOffset
	{
		set
		{
			m_f3PreviousScaleOffset = gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousScaleOffset, m_f3FinalScaleOffset - m_f3PreviousScaleOffset, m_fTimer, m_fDuration );
			m_f3FinalScaleOffset = value;
		}
	}

	protected override void InternalInit ()
	{
		base.InternalInit( );

		m_rCachedTransform = gameObject.transform;

		m_f3SavedPosition = m_rCachedTransform.localPosition;
		m_f3SavedRotation = m_rCachedTransform.localEulerAngles;
		m_f3SavedScale = m_rCachedTransform.localScale;

		m_f3PreviousPositionOffset = Vector3.zero;
		m_f3PreviousRotationOffset = Vector3.zero;
		m_f3PreviousScaleOffset = Vector3.zero;
	}

	protected override void EffectUpdate( )
	{
		Vector3 f3EulerAngles = gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousRotationOffset, m_f3FinalRotationOffset - m_f3PreviousRotationOffset, m_fTimer, m_fDuration );

		m_rCachedTransform.localRotation = Quaternion.Euler( m_f3SavedRotation + f3EulerAngles );
		m_rCachedTransform.localPosition = m_f3SavedPosition + gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousPositionOffset, m_f3FinalPositionOffset - m_f3PreviousPositionOffset, m_fTimer, m_fDuration );
		m_rCachedTransform.localScale = m_f3SavedScale + gkInterpolate.Ease( m_dEasingFunction, m_f3PreviousScaleOffset, m_f3FinalScaleOffset - m_f3PreviousScaleOffset, m_fTimer, m_fDuration );
	}
}
